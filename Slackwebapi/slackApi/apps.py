from django.apps import AppConfig


class SlackapiConfig(AppConfig):
    name = 'slackApi'
