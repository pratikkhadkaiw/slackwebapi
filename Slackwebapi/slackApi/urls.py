from django.urls import path

from slackApi.views import UserListAPIView

urlpatterns = [
    path('', UserListAPIView.as_view()),

]
