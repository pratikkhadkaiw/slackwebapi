from rest_framework import generics

from .models import UserDetails, AccountType
from .serializers import UserDetailSerializer, AccountSerializer


# Create your views here.

class UserListAPIView(generics.ListCreateAPIView):
    queryset = UserDetails.objects.all()
    serializer_class = UserDetailSerializer


class AccountListAPIView(generics.ListCreateAPIView):
    queryset = AccountType.objects.all()
    serializer_class = AccountSerializer
