from django.contrib import admin

# Register your models here.
from slackApi.models import AccountType, UserDetails

admin.site.register(AccountType)
admin.site.register(UserDetails)
