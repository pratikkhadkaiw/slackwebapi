from rest_framework import serializers
from rest_framework.serializers import ModelSerializer
from django.contrib.auth.forms import UserModel

from .models import AccountType, UserDetails


class AccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = AccountType
        fields = "__all__"


class UserSerializer(ModelSerializer):
    password = serializers.CharField(write_only=True)

    def create(self, validated_data):
        user = UserModel.objects.create(username=validated_data['username'])
        user.set_password(validated_data['username'])
        user.save()

        return user

    class Meta:
        model = UserModel
        fields = ("username", "password")


class UserDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserDetails
        fields = "__all__"
