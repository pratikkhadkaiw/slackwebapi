from django.contrib.auth.models import User
from django.db import models

ACCOUNT_TYPE_CHOICES = (
    ('Full Member', 'Full Member'),
    ('Admin', 'Admin'),
    ('Owner', 'Owner'),
    ('Guest', 'Guest'))

BILLING_STATUS_CHOICES = (
    ('active', 'active'),
    ('inactive', 'inactive')
)


class AccountType(models.Model):
    type = models.CharField("Account Type", max_length=25, choices=ACCOUNT_TYPE_CHOICES, blank=True, null=True)

    def __str__(self):
        return self.type


class UserDetails(models.Model):
    image = models.ImageField(upload_to="profile", blank=True, null=True)
    billing_status = models.CharField("Billing Status", max_length=10, choices=BILLING_STATUS_CHOICES, blank=True, null=True)
    account_type = models.CharField("Account Type", max_length=25, choices=ACCOUNT_TYPE_CHOICES, blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
